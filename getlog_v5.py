import os
import shutil

def write_app_log(request_id):
    application_log_file_source = open(os.path.join(path_log_local, 'application.log'), 'r') 
    application_log_lines = application_log_file_source.readlines() 

    path_save_new = os.path.join(path_save, case_name)
    if not os.path.exists(path_save_new):
        os.mkdir(os.path.join(path_save, case_name))
    path_application_log_file = os.path.join(path_save_new, f'{case_name}-application.txt')
    application_log_file = open(path_application_log_file, 'a', encoding='utf8')

    for line in application_log_lines:
        if request_id in line:
            application_log_file.write(line)

    application_log_file.close()

def write_performance_log(request_id):
    performance_log_file_source = open(os.path.join(path_log_local, 'performance.log'), 'r') 
    performance_log_lines = performance_log_file_source.readlines() 

    path_save_new = os.path.join(path_save, case_name)
    if not os.path.exists(path_save_new):
        os.mkdir(os.path.join(path_save, case_name))
    path_performance_log_file = os.path.join(path_save_new, f'{case_name}-performance.txt')
    performance_log_file = open(path_performance_log_file, 'a', encoding='utf8')

    for line in performance_log_lines:
        if request_id in line:
            performance_log_file.write(line)

    performance_log_file.close()

def write_operation_log(request_id):
    operation_log_file_source = open(os.path.join(path_log_local, 'operation.log'), 'r') 
    operation_log_lines = operation_log_file_source.readlines()

    path_save_new = os.path.join(path_save, case_name)
    if not os.path.exists(path_save_new):
        os.mkdir(os.path.join(path_save, case_name))
    path_operation_log_file = os.path.join(path_save_new, f'{case_name}-operation.txt')
    operation_log_file = open(path_operation_log_file, 'a', encoding='utf8')

    for index, line in enumerate(operation_log_lines):
        if request_id in line:
            operation_log_file.write(line)
            nextIndex = index + 1
            if line.split()[2] in ['ERROR','WARN'] and len(operation_log_lines) > nextIndex:
                if len(operation_log_lines[nextIndex].split("|")) < 3:
                    operation_log_file.write(operation_log_lines[nextIndex])
                    for i in range(index + 2, len(operation_log_lines)):
                        if "at" in operation_log_lines[i] or "Caused by" in operation_log_lines[i] or "more" in operation_log_lines[i]:
                            operation_log_file.write(operation_log_lines[i])
                        else:
                            break

    operation_log_file.close()

def write_sql_log(request_id):
    sql_log_file_source = open(os.path.join(path_log_local, 'sql.log'), 'r') 
    sql_log_lines = sql_log_file_source.readlines() 

    path_save_new = os.path.join(path_save, case_name)
    if not os.path.exists(path_save_new):
        os.mkdir(os.path.join(path_save, case_name))
    path_sql_log_file = os.path.join(path_save_new, f'{case_name}-sql.txt')
    sql_log_file = open(path_sql_log_file, 'a', encoding='utf8')

    for index, line in enumerate(sql_log_lines):
        if request_id in line:
            for i in range(index, index + 500):
                if sql_log_lines[i] in ['\n', '\r\n']:
                    for x in range(index, i):
                        sql_log_file.write(sql_log_lines[x])
                    sql_log_file.write('\n')
                    break

    sql_log_file.close()

if __name__ == "__main__":
    path_log_local = str(input('Input path log: '))
    path_save = str(input('Input path save file: '))
    while True:
        case_name = str(input('Input case name: '))
        request_ids = str(input('Input request id: '))

        if os.path.exists(os.path.join(path_save, case_name)):
            shutil.rmtree(os.path.join(path_save, case_name))

        for request_id in request_ids.split(','):
            write_app_log(request_id.strip())
            write_performance_log(request_id.strip())
            write_operation_log(request_id.strip())
            write_sql_log(request_id.strip())

        print('Generate log file successful!!!!')
